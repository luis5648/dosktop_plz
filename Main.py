from flask import Flask, request
from flask.json import jsonify
from werkzeug.exceptions import abort

app = Flask(__name__)

tasks = [
    {
        'uid': 1,
        'title': u'buy some',
        'desciption': u'some1 some2 some3 some4',
        'done': False
    },
    {
        'uid': 2,
        'title': u'send some',
        'desciption': u'some5 some6 some7 some8',
        'done': False
    }
]
@app.route('/')

def index():
	return "texto de prueba"

@app.route('/todo/api/v1.0/createTasks', methods=['POST'])
def create_task():
    if not request.json or not 'title' in request.json:
        abort(400)

    task = {
        'uid': tasks[-1]['uid']+1,
        'title': request.json['title'],
        'description': request.json.get('description',""),
        'done': False
    }
    tasks.append(task)
    return jsonify({'task': task}),201

@app.route('/todo/api/v1.0/listTasks', methods=['GET'])
def list_task():
    return jsonify({'task': tasks}),201

if __name__ == '__main__':

	app.run()

	app.run(debug=True)
